import XCTest
@testable import iTunesFavoriteApp

class TrackDetailsViewControllerTests: XCTestCase {
    // MARK: - System Under Test
    private var sut: TrackDetailsViewController!

    /// Esta parte es discutible, estas variables podrían no existir
    // MARK: - State
    private var nonFavoriteImage = UIImage(named: "favorite_no")
    private var favoriteImage = UIImage(named: "favorite")
    private var artistModelWithTrackName: ArtistModel = ArtistModel.clear(trackName: "Uprising")

    // MARK: - Test Life Cycle
    override func setUp() {
        super.setUp()

        sut = TrackDetailsViewController()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    // test_navigation_shouldHaveTitle
    // test_loadedView_shouldHaveTitle
    // test_whenViewIsLoaded_navigationShouldHaveTitle
    // test_whenViewIsLoaded_navigationTitleShouldBeSongs
    func test_whenViewIsLoaded_navigationTitleShouldBeTracksDetails() {
        // Arrange
        XCTAssertNil(sut.title, "precondition")

        // Act
        sut.loadViewIfNeeded()

        // Assert
        XCTAssertEqual(sut.title, "Tracks Details")
        // Nuestra vista tiene bastante funcionalidad y tenemos suerte de que pase a la primera
        // Sin embargo, podemos esperar side effects como consecuencia de llamar al método anterior
    }

    func test_whenViewIsLoaded_outletsShouldBeConnected() {
        // Act
        sut.loadViewIfNeeded()

        // Assert
        XCTAssertNotNil(sut.trackImageView)
        XCTAssertNotNil(sut.favoriteButton)
        XCTAssertNotNil(sut.detailsLabel)
    }

    func test_whenViewIsLoaded_defaultBackgroundImageForFavoriteButtonShouldBeNonFavorite() {
        // Act
        sut.loadViewIfNeeded()

        // Assert
        XCTAssertEqual(sut.favoriteButton.currentBackgroundImage, nonFavoriteImage)
    }

    /// Collaboration Scenarios

    func test_detailsLabel_shouldShowTheArtistModelTrackName() {
        // Arrange
        sut.loadViewIfNeeded()

        // Act
        sut.display(artist: ArtistModel.clear(trackName: "Uprising"))

        // Assert
        XCTAssertEqual(sut.detailsLabel.text, "Uprising")
    }

    func test_whenTrackIsNotFavorite_favoriteButtonShouldDisplayANonFavoriteBackgroundImage() {
        // Arrange
        sut.loadViewIfNeeded()
        sut.display(isFavorite: true)
        XCTAssertEqual(sut.favoriteButton.currentBackgroundImage, favoriteImage)

        // Act
        sut.display(isFavorite: false)

        // Assert
        XCTAssertEqual(sut.favoriteButton.currentBackgroundImage, nonFavoriteImage)
    }

    func test_whenTrackIsFavorite_favoriteButtonShouldDisplayAFavoriteBackgroundImage() {
        // Arrange
        sut.loadViewIfNeeded()

        // Act
        sut.display(isFavorite: true)

        // Assert
        XCTAssertEqual(sut.favoriteButton.currentBackgroundImage, favoriteImage)
    }

    /// Collaborations

    func test_whenViewIsLoaded_loadShouldBeCalledOnPresenter() {
        // Arrange
        let presenterSpy = TrackDetailsPresenterSpy()
        sut.presenter = presenterSpy

        // Act
        sut.loadViewIfNeeded()

        // Assert
        XCTAssertTrue(presenterSpy.loadWasCalled)
    }

    func test_tappingFavoriteButton_whenTrackIsNotFavorite_makeTrackFavoriteShouldBeCalledOnPresenter() {
        // Arrange
        let presenterSpy = TrackDetailsPresenterSpy()
        sut.presenter = presenterSpy
        sut.loadViewIfNeeded()
        sut.display(isFavorite: false)

        // Act
        tap(sut.favoriteButton)

        // Assert
        XCTAssertTrue(presenterSpy.makeTrackFavoriteWasCalled)
    }

    func test_tappingFavoriteButton_whenTrackIsFavorite_deleteTrackFromFavoritesShouldBeCalledOnPresenter() {
        // Arrange
        let presenterSpy = TrackDetailsPresenterSpy()
        sut.presenter = presenterSpy
        sut.loadViewIfNeeded()
        sut.display(isFavorite: true)

        // Act
        tap(sut.favoriteButton)

        // Assert
        XCTAssertTrue(presenterSpy.deleteTrackFromFavoritesWasCalled)
    }

    func test_whenViewDidAppear_reloadShouldBeCalledOnPresenter() {
        // Act
        let presenterSpy = TrackDetailsPresenterSpy()
        sut.presenter = presenterSpy
        sut.viewWillAppear(false)

        XCTAssertTrue(presenterSpy.reloadWasCalled)
    }


}
