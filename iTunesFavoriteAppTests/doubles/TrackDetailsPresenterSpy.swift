@testable import iTunesFavoriteApp

class TrackDetailsPresenterSpy: TrackDetailsPresenterProtocol {
    var view: TrackDetailsViewProtocol?
    var interactor: TrackDetailsInteractorProtocol?
    var router: TrackDetailsRouterProtocol?

    func load() {
        loadWasCalled = true

        // EN la versión real, el presenter llamaría de vuelta a la vista pasandole un modelo
    }

    func reload() {
        reloadWasCalled = true
    }

    func makeTrackFavorite() {
        makeTrackFavoriteWasCalled = true
    }

    func deleteTrackFromFavorites() {
        deleteTrackFromFavoritesWasCalled = true
    }

    // MARK: - Spy
    var loadWasCalled = false
    var reloadWasCalled = false
    var makeTrackFavoriteWasCalled = false
    var deleteTrackFromFavoritesWasCalled = false

}
