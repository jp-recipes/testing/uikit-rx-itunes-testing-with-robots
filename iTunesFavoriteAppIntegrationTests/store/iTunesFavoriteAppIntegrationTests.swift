import XCTest
import RxSwift
import RxBlocking
@testable import iTunesFavoriteApp

class iTunesFavoriteAppIntegrationTests: XCTestCase {
    func test_savingTrackAsFavorite_shouldBeRetrievedFromLocalStore() throws {
        // Arrange
        let artistModel = ArtistModel.clear(trackId: 123, trackName: "Uprising")
        let dataStack = CoreDataStack(modelName: "iTunesModel")
        let localStore = CoreDataLocalStore(stack: dataStack)
        let repository = LocalFavoriteArtistRepository(localStore: localStore)
        let interctor = TrackDetailsInteractor(localFavoriteRepository: repository)
        let view = TrackDetailsViewController()
        let presenter = TrackDetailsPresenter(artistModel: artistModel)
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interctor
        interctor.delegate = presenter
        view.loadViewIfNeeded()

        /// Deal with Asynchronous Code: Create an expectation
        expectation(forNotification: Notification.Name.NSManagedObjectContextDidSave, object: nil, handler: nil)

        // Act
        tap(view.favoriteButton)

        /// Deal with Asynchronous Code: Wait for that expectation to occur
        waitForExpectations(timeout: 0.1, handler: nil)

        // Assert
        let storedTrack = try localStore.fetchArtist(by: Int32(artistModel.trackId)).map(TrackEntity.toArtistModel).toBlocking().single()
        XCTAssertEqual(storedTrack.trackName, "Uprising")
    }
}
