import Foundation

final class SearchRobot: Robot {
    private let screenTitle = "Search Tracks"

    private lazy var title      = app.navigationBars.staticTexts[screenTitle]
    private lazy var searchBar  = app.searchFields.firstMatch
    private lazy var trackCells = app.tables.cells

    @discardableResult
    func checkScreen() -> Self {
        assert(title, [.contains(screenTitle)])

        return self
    }

    @discardableResult
    func searchTracksByArtist(name: String) -> Self {
        tap(searchBar)
        searchBar.typeText(name)

        return self
    }

    @discardableResult
    func seeTrackDetails(byName trackName: String) -> TrackRobot {
        tap(trackCells.staticTexts[trackName].firstMatch)

        return TrackRobot(app)
    }
}
