import Foundation

final class FavoriteRobot: Robot {
    @discardableResult
    func checkTrack(name: String) -> Self {
        assert(app.staticTexts[name], [.exists])
    }
}
