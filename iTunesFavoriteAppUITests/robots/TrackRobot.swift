import Foundation

final class TrackRobot: Robot {
    private let screenTitle = "Tracks Details"

    private lazy var albumImage     = app.images.firstMatch
    private lazy var favoriteButton = app.buttons["save"].firstMatch
    private lazy var favoriteTabBar = app.tabBars.buttons["Favorites"]

    @discardableResult
    func checkAlbumImage() -> Self {
        assert(albumImage, [.exists])

        return self
    }

    @discardableResult
    func toggleFavorite() -> Self {
        tap(favoriteButton)

        return self
    }

    @discardableResult
    func seeFavorites() -> FavoriteRobot {
        tap(favoriteTabBar)

        return FavoriteRobot(app)
    }
}
