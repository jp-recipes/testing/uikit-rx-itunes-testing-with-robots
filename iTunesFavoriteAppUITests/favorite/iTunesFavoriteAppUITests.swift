import XCTest
@testable import iTunesFavoriteApp

class iTunesFavoriteAppUITests: XCTestCase {

    private var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        app = XCUIApplication()
        app.launchArguments = ["--reset"]
        app.launch()
    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }

    func test_whenSearchingAnArtistAndMarkingATrackAsFavorite_shouldBeAccesibleFromTheTabFavorites1() {
        // View 1
        let searchField = app.searchFields.firstMatch
        searchField.tap()
        searchField.typeText("Muse")

        let firstCell = app.tables.cells.staticTexts["Uprising"].firstMatch
        firstCell.tap()

        // View 2
        _ = app.navigationBars.staticTexts["Track Details"].waitForExistence(timeout: 1)
        let favoriteButton = app.buttons["save"]
        favoriteButton.tap()

        app.tabBars.buttons["Favorites"].tap()

        // View 3
        _ = app.navigationBars.staticTexts["Favorites"].waitForExistence(timeout: 1)

        XCTAssertTrue(app.staticTexts["Uprising"].waitForExistence(timeout: 0.1))
    }

    func test_whenSearchingAnArtistAndMarkingATrackAsFavorite_shouldBeAccesibleFromTheTabFavorites2() {
        SearchRobot(app)
            .checkScreen()
            .searchTracksByArtist(name: "Muse")
            .seeTrackDetails(byName: "Madness")
            .checkAlbumImage()
            .toggleFavorite()
            .seeFavorites()
            .checkTrack(name: "Madness")
    }
}

