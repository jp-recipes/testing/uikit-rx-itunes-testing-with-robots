import Foundation
import CoreData

@objc(TrackEntity)
public class TrackEntity: NSManagedObject {
    static func toArtistModel(_ trackEntity: TrackEntity) -> ArtistModel {
        ArtistModel(wrapperType: trackEntity.wrapperType,
                    collectionId: trackEntity.collectionId,
                    trackId: Int(trackEntity.trackId),
                    artistName: trackEntity.artistName,
                    collectionName: trackEntity.collectionName,
                    trackName: trackEntity.trackName,
                    previewUrl: trackEntity.previewUrl,
                    artworkUrl100: trackEntity.artworkUrl100
        )
    }
}
