import Foundation

protocol ApplicationFactoryProtocol {
    func artistRepository() -> RemoteArtistRepositoryProtocol
    func localRepository() -> LocalFavoriteArtistRepositoryProtocol
}

class ApplicationFactory {
    private let environment: Environment

    private lazy var dataStack = CoreDataStack(modelName: "iTunesModel")

    private lazy var session: URLSession = {
        let noCacheConfig = URLSessionConfiguration.default
        noCacheConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        noCacheConfig.urlCache = nil

        return URLSession(configuration: noCacheConfig)
    }()

    private lazy var client: ApiClient = {
        let url = URL(string: environment.urlString)!
        return RestClient(manager: session, url: url)
    }()

    init(environment: Environment) {
        self.environment = environment
    }
}
extension ApplicationFactory: ApplicationFactoryProtocol {
    func artistRepository() -> RemoteArtistRepositoryProtocol {
        return RemoteArtistRepository(api: client)
    }

    func localRepository() -> LocalFavoriteArtistRepositoryProtocol {
        let localStore = CoreDataLocalStore(stack: dataStack)

        return LocalFavoriteArtistRepository(localStore: localStore)
    }
}
