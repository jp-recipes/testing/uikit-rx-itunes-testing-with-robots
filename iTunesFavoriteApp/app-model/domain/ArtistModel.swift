import Foundation

struct ArtistModel: Decodable {
    let wrapperType: String
    let collectionId: Int
    let trackId: Int
    let artistName: String
    let collectionName: String
    let trackName: String?
    let previewUrl: String?
    let artworkUrl100: String
    
    static func empty() -> ArtistModel {
        return clear()
    }

    static func clear(
            wrapperType: String = "",
            collectionId: Int = 0,
            trackId: Int = 0,
            artistName: String = "",
            collectionName: String = "",
            trackName: String? = nil,
            previewUrl: String? = nil,
            artworkUrl100: String = "") -> ArtistModel {
        return ArtistModel(
            wrapperType: wrapperType,
            collectionId: collectionId,
            trackId: trackId,
            artistName: artistName,
            collectionName: collectionName,
            trackName: trackName,
            previewUrl: previewUrl,
            artworkUrl100: artworkUrl100
        )
    }
}
