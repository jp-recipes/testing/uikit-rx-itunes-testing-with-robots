import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    private let environment = Environment(urlString: "https://itunes.apple.com")

    private var initializer: AppInitializable {
        ApplicationController(factory: ApplicationFactory(environment: environment))
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)

        if CommandLine.arguments.contains("--reset") {
            UIView.setAnimationsEnabled(false)
            let coreDataStack = CoreDataStack(modelName: "iTunesModel")
            coreDataStack.clear()
        }

        initializer.start(with: window!)

        return true
    }
}

