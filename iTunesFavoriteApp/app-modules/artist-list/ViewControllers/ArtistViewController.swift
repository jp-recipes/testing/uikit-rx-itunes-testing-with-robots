import UIKit
import RxSwift
import RxCocoa

class ArtistViewController: UIViewController, HudDisplayable {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: - Dependencies
    var presenter: ArtistListPresenterProtocol?
    var hud: HudWrapper?

    // MARK: - Rx
    private let items = BehaviorSubject<[ArtistModel]>(value: [])
    private let favorites = BehaviorSubject<[ArtistModel]>(value: [])

    private let bag = DisposeBag()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Search Tracks"
        
        setupUI()
    }

    private func setupUI() {
        searchBar.rx.text.orEmpty.changed
            .filter { !$0.isEmpty }
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] text in
                self?.presenter?.searchArtists(by: text)
            })
            .disposed(by: bag)

        items
            .asObservable()
            .bind(to: tableView.rx.items) { (tableView: UITableView, index: Int, element: ArtistModel) in
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")

                cell.textLabel?.text = element.trackName

                return cell
            }
            .disposed(by: bag)
        
        tableView.rx.itemSelected
            .map { try self.items.value()[$0.row] }
            .subscribe(onNext: { item in
                self.presenter?.selected(track: item)
            })
            .disposed(by: bag)
    }
    
    
}
extension ArtistViewController: ArtistListViewProtocol {
    func display(artists: [ArtistModel]) {
        items.onNext(artists)
    }
}
